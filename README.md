# Supplier Gem

### Capfile content:


```
#!ruby

load 'deploy'
load 'config/deploy'
```



### Gemfile content:


```
#!ruby

group :deployment do
  gem 'supplier', git: 'https://bitbucket.org/ikitlab/supplier.git'
end

```


### deploy/<stage>.rb content


```
#!ruby

set :branch,        'master'
set :domain,        '123.123.123.123'

role :web,          "#{user}@#{domain}"
role :app,          "#{user}@#{domain}"
role :db,           "#{user}@#{domain}", :primary => true

```


### deploy.rb content


```
#!ruby

set :application, 'prj'
set :more_stages, %w(development production)
set :more_shared_settings, %w(social_apps.yml)
set :more_shared_children, %w(public/top)

require 'supplier/capistrano'

namespace :deploy do
  task :restart do
    run "cd #{release_path} && touch tmp/restart.txt"
    run "supervisorctl restart prj:*"
  end
end


```