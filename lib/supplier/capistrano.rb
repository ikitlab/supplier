require 'capistrano_colors'
require 'bundler/capistrano'

Capistrano::Configuration.instance.load do
  # we use assets compilation, sometimes
  unless fetch(:no_assets, false)
    load 'deploy/assets'
  end

  ## COMMON DEFINITIONS

  # ssh settings
  # possible options from here: http://net-ssh.github.io/net-ssh/classes/Net/SSH.html
  ssh_options[:auth_methods]          = ["publickey"]
  ssh_options[:keys]                  = [ENV['SSH_KEY_FILE']] unless ENV['SSH_KEY_FILE'].nil?
  ssh_options[:forward_agent]         = true
  ssh_options[:paranoid]              = false
  ssh_options[:port]                  = 22
  default_run_options[:pty]           = true

  # identification
  set(:user)                          { application }

  # repository
  set(:scm)                           { :git }
  set(:scm_verbose)                   { false }
  set(:deploy_via)                    { :remote_cache }
  set(:copy_exclude)                  { [ '.git' ] }

  set(:repository)                    { "git@bitbucket.org:ikitlab/#{application}.git" }
  set(:branch)                        { "master" }

  # directories
  set(:deploy_to)                     { "/srv/#{application}" }
  set(:shared_children)               { (fetch(:more_shared_children, []) + %w(log tmp/pids tmp/cache tmp/sockets public/system public/assets public/uploads)).uniq }

  # other settings
  set(:rails_env)                     { "production" }
  set(:rack_env)                      { "production" }
  set(:use_sudo)                      { false }
  set(:keep_releases)                 { 10 }
  set(:rake)                          { "RAILS_ENV=#{rails_env} RACK_ENV=#{rack_env} #{fetch(:bundle_cmd, 'bundle')} exec rake --trace" }
  set(:normalize_asset_timestamps)    { false }

  # shared settings
  set(:shared_settings)               { (fetch(:more_shared_settings, []) + %w(database.yml)).uniq }


  ## STAGES
  set(:stages)                    { (fetch(:more_stages, []) + %w(development staging production)).uniq }

  # Should define stage tasks as soon as posible, before any option in this file is set
  fetch(:stages).each do |name|
    desc "Set the target stage to `#{name}'."
    task(name) do
      set :stage,   name.to_sym
      # set :domain,  'example.com'
      if fetch(:stages).include?(name)
        # load stages from files
        load "config/deploy/#{name}" if file_in_load_path?("config/deploy/#{name}")
      else
        abort "Undefined stage #{name}"
      end
    end
  end

  on :load do
    # The first non option argument
    puts "Available stages: #{fetch(:stages)}"
    env = ARGV.detect { |a| a.to_s !~ /\A-/ && a.to_s !~ /=/ }
    if fetch(:stages).include?(env)
      # Execute the specified stage so that recipes required in stage can contribute to task list
      find_and_execute_task(env) if ARGV.any?{ |option| option =~ /-T|--tasks|-e|--explain/ }
    else
      abort 'Wrong stage specified.'
    end
  end


  ## HOOKS
  # ensure we have all necessary directories in the shared folder
  before "deploy:update", "deploy:setup"

  # copy secured configuration files from shared/ dir to release
  after "deploy:finalize_update", roles: :app do
    shared_settings.each do |f|
      run <<-CMD
        if [ -f #{shared_path}/config/#{f} ] || [ -d #{shared_path}/config/#{f} ]; then \
          rm -f #{release_path}/config/#{f}; \
          mkdir -p #{File.dirname(File.join(release_path, 'config', f))}; \
          cp -a #{shared_path}/config/#{f} #{release_path}/config/#{f}; \
        else \
          echo '#{f} does not exist!'; \
          false; \
        fi
      CMD
    end
  end

  # set build version
  after "deploy:create_symlink",  roles: :app do
    run "echo BUILD_VERSION = \\'#{fetch(:revision, 'n/a')}\\' > #{release_path}/config/initializers/version.rb"
  end

  namespace :deploy do
    desc "Load the seed data"
    task :seed do
      run "cd #{release_path}; #{rake} db:seed"
    end
  end

  # triggering callbacks

  unless fetch(:no_db, false)
    after "deploy:update_code", "deploy:migrate"
    after "deploy:update_code", "deploy:seed"
  end

  after "deploy:restart",     "deploy:cleanup"

end
