# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'supplier/version'

Gem::Specification.new do |spec|
  spec.name          = "supplier"
  spec.version       = Supplier::VERSION
  spec.authors       = ["Ilya Kraislnikov"]
  spec.email         = ["ilya@ikitlab.com"]
  spec.description   = %q{ikitlab projects supplying gem}
  spec.summary       = %q{ikitlab projects supplying gem}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_dependency "capistrano", "< 3.0"
  spec.add_dependency "capistrano_colors"

  # because of this bug with Jenkins: http://jasiek.me/2013/11/27/jenkins-capistrano-ssh-agent-plugin.html
  spec.add_dependency "net-ssh", ">=2.9.1"

end
